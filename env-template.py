username      = '' # Cloud admin username
password      = '' # Cloud app password
workspace     = '' # Cloud workspace slug/id (as seen in the url)
uuid          = '' # User's UUID that will be added to all branch permissions
email         = '' # User's email that will be added to all branch permissions
name          = '' # User's name that will be added to all branch permissions
patterns      = ['master', 'production'] # Fill in this array with as many branch patterns you wish to have this user added to