from numpy import array
from requests import Session
from typing import Generator
from dataclasses import dataclass
from time import sleep

try:
    import env
    username     = env.username
    password     = env.password
    WORKSPACE    = env.workspace
    UUID         = env.uuid
    EMAIL        = env.email
    name         = env.name
    patterns     = env.patterns

    SESSION      = Session()
    SESSION.auth = (username, password)
    API_URL      = "https://api.bitbucket.org"
except [ImportError, AttributeError]:
    print('Could not locate one or more attributes within the "env.py" file. '
          'Please follow the readme to ensure that all attributes are present and try again.')
    exit()


@dataclass
class Repo:
    name: str
    slug: str

@dataclass
class User:
    public_name: str
    uuid: str
    formatted: dict = None

    def __post_init__(self):
        self.format_user()

    def format_user(self):
        formated_user = {
                         "type": "user",
                         "uuid": self.uuid
                        }
        self.formatted = formated_user

@dataclass
class Group:
    slug: str
    formatted: dict = None

    def __post_init__(self):
        self.format_group()

    def format_group(self):
        formated_group = {
                         "type": "group",
                         "slug": self.slug
                        }
        self.formatted = formated_group

@dataclass
class BranchPermission:
    '''
    kind of "branching_model" requires self.branch_type
    kind of "glob" requires self.pattern
    '''
    kind: str
    # "branching_model" or "glob"
    restriction_kind: str
    users: list
    groups: list
    branch_type: str
    pattern: str
    _id: str

    def __hash__(self):
        return hash(f'{self.kind}:{self.branch_type or self.pattern}')

    def __post_init__(self):
        self.user_cleanup()
        self.group_cleanup()

    def user_cleanup(self):
        temp_list = []
        for user in self.users:
            new_user = User(user.get('display_name'), user.get('uuid'))
            temp_list.append(new_user)
        self.users = temp_list
    def group_cleanup(self):
        temp_list = []
        for group in self.groups:
            new_group = Group(group.get('slug'))
            temp_list.append(new_group)
        self.groups = temp_list


def get_api(endpoint, params):
    headers = {'Accept': 'application/json', 'Content-type': 'application/json'}
    r = SESSION.get(endpoint, params=params, headers=headers)
    if r.status_code == 429:
        print('WARN: Hit api rate limit, sleeping for 15 seconds then attempting to resume...')
        sleep(15)
    return r

def put_api(endpoint, payload):
    headers = {'Accept': 'application/json', 'Content-type': 'application/json'}
    r = SESSION.put(endpoint, json=payload, headers=headers)
    if r.status_code == 429:
        print('WARN: Hit api rate limit, sleeping for 15 seconds then attempting to resume...')
        sleep(15)
    return r

def get_repos(page=None, limit=1_000) -> Generator[Repo, None, None]:
    # https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Bworkspace%7D
    while True:
        params = {'page': page, 'limit': limit}
        endpoint = f'{API_URL}/2.0/repositories/{WORKSPACE}'
        r = get_api(endpoint, params)
        r_json = r.json()
        for raw_repo in r_json.get('values'):
            repo = Repo(raw_repo.get('name'), raw_repo.get('slug'))
            yield repo
        
        if page == None:
            page = 1
        
        if r_json.get('next'):
            page += 1
        else:
            return


def get_repo_branch_permissions(repo: Repo, page=None, limit=1_000) -> Generator[BranchPermission, None, None]:
    # https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Bworkspace%7D/%7Brepo_slug%7D/branch-restrictions#get
    while True:
        params = {'page': page, 'limit': limit}
        endpoint = f'{API_URL}/2.0/repositories/{WORKSPACE}/{repo.slug}/branch-restrictions'
        r = get_api(endpoint, params)
        r_json = r.json()
        for value in r_json.get('values'):
            branch_permission = BranchPermission(kind=value.get('branch_match_kind'),
                                                 restriction_kind=value.get('kind'),
                                                 users=value.get('users'),
                                                 groups=value.get('groups'),
                                                 branch_type=value.get('branch_type'),
                                                 pattern=value.get('pattern'),
                                                 _id=value.get('id'))
            yield branch_permission

        if page == None:
            page = 1

        if r_json.get('next'):
            page += 1
        else:
            return


def add_user(repo: Repo) -> bool:
    endpoint = f"https://bitbucket.org/api/internal/privileges/{env.workspace}/{repo.slug}/{EMAIL}"
    headers = {'Accept': 'application/json', 'Content-type': 'application/json'}
    r = SESSION.put(endpoint, data="write")
    if r.status_code == 429:
        print('WARN: Hit api rate limit, sleeping for 15 seconds then attempting to resume...')
        sleep(15)
    elif r.status_code in [200, 201, 202]:
        return True
    else:
        print(f"Something went wrong adding the user to the repository {repo.slug} - {r.status_code} {r.text}")
        return False


def update_branch_permission(branch_permission: BranchPermission, repo: Repo, payload=None) -> bool:
    # https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Bworkspace%7D/%7Brepo_slug%7D/branch-restrictions#post
    endpoint = f'{API_URL}/2.0/repositories/{WORKSPACE}/{repo.slug}/branch-restrictions/{branch_permission._id}'

    r = put_api(endpoint, payload)

    if r.status_code in [200, 201]:
        return True
    elif r.status_code == 400:
        print(r.text.get('error'))
        return False
    else:
        return False

def main() -> None:
    for repo in get_repos():
        repo_branch_patterns: set[BranchPermission] = set()
        
        for branch_permission in get_repo_branch_permissions(repo):
            repo_branch_patterns.add(branch_permission)
    
        for branch_permission in repo_branch_patterns:
            if branch_permission.restriction_kind == "push" and branch_permission.pattern in patterns:
                if add_user(repo):
                    payload = {
                                "users": list(),
                                "groups": list()
                              }
                    for user in branch_permission.users:
                        payload['users'].append(user.formatted)
                    for group in branch_permission.groups:
                        payload['groups'].append(group.formatted)
                    new_user = User(name, UUID)
                    payload['users'].append(new_user.formatted)
                    if not update_branch_permission(branch_permission, repo, payload):
                        print(f'WARN: Failed to update branch permission for repo: "{repo.name}" with pattern "{branch_permission.kind}:{branch_permission.branch_type or branch_permission.pattern}".')
                    else:
                        print(f'INFO: Successfully updated branch permissions: "{branch_permission.kind}:{branch_permission.branch_type or branch_permission.pattern}" in repo: "{repo.name}"')


if __name__ == '__main__':
    main()